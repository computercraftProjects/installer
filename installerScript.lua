progs={"pastebin get P9dDhQ2m stdui","pastebin get 7j7edPiL btn","pastebin get Ppte9dLM buttonUI","wget https://gitlab.com/computercraftProjects/buttonapi/glassos/-/raw/master/glassui.lua glassui","wget https://gitlab.com/computercraftProjects/buttonapi/glassos/-/raw/master/shatter.lua shatter","wget https://gitlab.com/computercraftProjects/buttonapi/glassos/-/raw/master/shatterglasses.lua shatterglasses"}

prognames={"stdGUI","buttonapi","buttonUI","glassui","shatter","shatterglasses"}--try to keep updated
--add an empty entry if you dont know what to call it

function slowPrint (char,time,maxI,donetext)
	for i=0,maxI do
		write(char)
		sleep(time/maxI)
	end
	print("")
	print(donetext)
end

function prompt (prog,progName)
	print("do you want to download and install "..progName.." ?(y/n)")
	write(">")
	install=read()
	if install=="y" then
--"bg "..
		shell.run(prog..".lua")
		slowPrint(".",1,8,"done!")
	elseif install=="n" then
		print("okay")
		return
	else
		print("must be one of \"y\" or \"n\"")
		prompt(prog, progName)
	end
end

for k,v in pairs(progs) do
	prompt(v, prognames[k])
end
